package assign.resources;

import java.util.Map;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import assign.services.OpenStackService;


/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 6: JavaScript and Cloud Computing
 * 
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

@Path("/meetings")
public class OpenStackResource {

  OpenStackService openStackService;

  public OpenStackResource() {
    openStackService = new OpenStackService();
  }

  @GET
  @Path("/solum_team_meeting")
  @Produces("text/html")
  public String getSolumTeamMeetingCounts() {

    Map<String, Integer> countMap = openStackService.getMeetingCounts();

    StringBuilder sb = new StringBuilder();

    for (Map.Entry<String, Integer> entry : countMap.entrySet()) {
      String year = entry.getKey();
      Integer numMeetings = entry.getValue();
      sb.append(year + " " + numMeetings + " ");
    }

    return sb.toString();
  }
}
