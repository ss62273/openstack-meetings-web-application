package assign.resources;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 6: JavaScript and Cloud Computing
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

@ApplicationPath("/openstack")
public class OpenStackApplication extends Application {

  private Set<Object> singletons = new HashSet<Object>();
  private Set<Class<?>> classes = new HashSet<Class<?>>();

  public OpenStackApplication() {}

  @Override
  public Set<Class<?>> getClasses() {
    classes.add(OpenStackResource.class);
    return classes;
  }

  @Override
  public Set<Object> getSingletons() {
    return singletons;
  }
}
