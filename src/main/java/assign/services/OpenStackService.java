package assign.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 6: JavaScript and Cloud Computing
 * 
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

public class OpenStackService {

  private static final String SOLUM_TEAM_MEETING_URL =
      "http://eavesdrop.openstack.org/meetings/solum_team_meeting/";
  private static final String[] SKIP_CASE =
      {"Name", "Last modified", "Size", "Description", "Parent Directory"};

  public OpenStackService() {
    try {
      Jsoup.connect(SOLUM_TEAM_MEETING_URL).timeout(10000).execute();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Uses meeting file name to extract date and count # of distinct meetings
   * 
   * @return Map of year and # of meetings for each year
   */
  public Map<String, Integer> getMeetingCounts() {
    Map<String, Integer> countMap = new TreeMap<String, Integer>();
    List<String> dateList;

    try {
      Document document = Jsoup.connect(SOLUM_TEAM_MEETING_URL).get();
      // Get all links
      Elements years = document.getElementsByAttribute("href");
      if (years != null && !years.isEmpty()) {
        for (Element e : years) {
          String year = e.html();
          // Filter out unnecessary links and only keep years
          if (!Arrays.asList(SKIP_CASE).contains(year)) {
            document = Jsoup.connect(e.attr("abs:href")).get();
            // Get all file names
            Elements fileNames = document.getElementsByAttribute("href");
            if (fileNames != null && !fileNames.isEmpty()) {
              dateList = new ArrayList<String>();
              for (Element f : fileNames) {
                // Filter out unnecessary links and only keep file names
                if (!Arrays.asList(SKIP_CASE).contains(f.html())) {
                  // Extract file date from file name
                  String fileDate = f.html().replaceAll("[^\\d-]", "");
                  // Add file date to dateList to count # of distinct meetings
                  if (!dateList.contains(fileDate)) {
                    dateList.add(fileDate);
                  }
                }
              }
              // Put # of meetings for each year into countMap
              year = year.replaceAll("[^\\d]", "");
              countMap.put(year, dateList.size());
            }
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return countMap;
  }

}
