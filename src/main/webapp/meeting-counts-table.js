/**
 * 
 */

function createMeetingCountsTable() {
	var xhttp = new XMLHttpRequest();
	xhttp.open("GET", "/openstack/meetings/solum_team_meeting", false);
	xhttp.send();

	var countData = xhttp.response.split(" ");
	
	var countTable = "<tr><th>Year</th><th>Number of meetings</th></tr>";

	var i;
	for (i = 0; i < countData.length - 1; i += 2) {
		var year = countData[i];
		var numMeetings = countData[i + 1];
		countTable += "<tr><td>" + year + "</td><td>" + numMeetings
				+ "</td></tr>";
	}

	document.getElementById("counts-table").border = 1;
	document.getElementById("counts-table").innerHTML = countTable;
}